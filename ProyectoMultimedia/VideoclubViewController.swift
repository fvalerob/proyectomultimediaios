//
//  VideoclubViewController.swift
//  ProyectoMultimedia
//
//  Created by Fran on 6/2/18.
//  Copyright © 2018 Fran. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer

class VideoclubViewController: UITableViewController {

    var player : AVPlayer?
    var listaVideos : [UAMedio]!
    var isPlay : Bool!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.listaVideos = UAFuenteDatos.datos.listaVideos
        self.isPlay = false
        
        var audioSession = AVAudioSession.sharedInstance()
        var error: Error? = nil
        
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(true)
        }
        catch let error {
        }
        
        
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.listaVideos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellVideo", for: indexPath)
        
        let object = self.listaVideos[indexPath.row]
        cell.textLabel!.text = object.titulo
        cell.detailTextLabel!.text = object.artista
        // Configure the cell...
        
        return cell
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellVideo", for: indexPath)
        
        let object = self.listaVideos[indexPath.row]
        if (self.isPlay == false){
            
            let videoURL = NSURL(string: object.url.absoluteString)
            var controller = AVPlayerViewController()
            self.player = AVPlayer(url: videoURL! as URL)
            controller.view.backgroundColor = UIColor.blue
            // Image
            var imageView = UIImageView(image: UIImage(named: "fondo.png"))
            imageView.frame = (controller.contentOverlayView?.frame)!
            controller.contentOverlayView?.addSubview(imageView)
            
            controller.player = self.player
            self.present(controller, animated: true, completion: { () })
            
            self.isPlay = true
            cell.setSelected(true,animated:true)
        }else{
            if (self.isPlay == true){
                //self.player.stop()
                self.isPlay = false
                cell.setSelected(false,animated:false)
            }
        }
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
}

