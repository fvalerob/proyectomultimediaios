//
//  RadioTableViewCell.swift
//  ProyectoMultimedia
//
//  Created by Fran on 10/2/18.
//  Copyright © 2018 Fran. All rights reserved.
//
import Foundation
import UIKit

class RadioTableViewCell: UITableViewCell {

    @IBOutlet weak var imagen: UIImageView!
    
    @IBOutlet weak var titulo: UILabel!
    
    @IBOutlet weak var artista: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
