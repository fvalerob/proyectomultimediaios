//
//  RadioViewController.swift
//  ProyectoMultimedia
//
//  Created by Fran on 6/2/18.
//  Copyright © 2018 Fran. All rights reserved.
//

import UIKit
import AVFoundation


class RadioViewController: UITableViewController {
    
    var musicPlayer = AVAudioPlayer()
    var listaAudios : [UAMedio]!
    var isPlay : Bool!
    var player : AVAudioPlayer!
    var indexItemPulsado : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.listaAudios = UAFuenteDatos.datos.listaAudios
        self.isPlay = false
        self.indexItemPulsado = -1
        
        var audioSession = AVAudioSession.sharedInstance()
        var error: Error? = nil
        
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(true)
        }
        catch let error {
        }
        
        //CONTROL DESCONEXION DE AURICULARES
        var nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(self.routeChanged), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
        
    }
    
    @objc func routeChanged(_ sender: Any) {
        if self.player.isPlaying {
            self.player.pause()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.listaAudios.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellRadio", for: indexPath)
        
        let object = self.listaAudios[indexPath.row]
        
        cell.textLabel!.text = object.titulo
        cell.detailTextLabel!.text = object.artista
        
        if(self.indexItemPulsado == indexPath.row){
            if (self.isPlay == false){
                cell.imageView?.image = UIImage(named: "play-button.png")
            }else{
                cell.imageView?.image = UIImage(named: "stop-button.png")
            }
        }
        return cell
    }
    
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let cell = self.tableView.dequeueReusableCell(withIdentifier: "CellRadio", for: indexPath) as! RadioTableViewCell
      
        let object = self.listaAudios[indexPath.row]
        //Guardomos el indexPath de la celda seleccionada.
        indexItemPulsado = indexPath.row
        
        if (self.isPlay == false){
            self.player = try! AVAudioPlayer(contentsOf: object.url)
            self.player.prepareToPlay()
            self.player.play()
            self.isPlay = true
        }else{
            if (self.isPlay == true){
                self.player.stop()
                self.isPlay = false
            }
        }
        self.tableView.reloadData()
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

 }
