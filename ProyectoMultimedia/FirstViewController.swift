//
//  FirstViewController.swift
//  ProyectoMultimedia
//
//  Created by Fran on 20/12/17.
//  Copyright © 2017 Fran. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class FirstViewController: UIViewController {
    
    var captureSession : AVCaptureSession?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.crearVista()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(comprobarPosicion), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    @objc func comprobarPosicion() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            self.crearVista()
        }
        else if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            self.crearVista()
        }
    }
    func crearVista(){
        // Creamos vista y añadimos a  la vista actual
        let pieChartView = PieChartView()
        pieChartView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        pieChartView.segments = [
            Segment(color: UIColor.red, value: CGFloat(UAFuenteDatos.datos.listaEmisiones.count), texto: "Vídeo en directo"),
            Segment(color: UIColor.blue, value: CGFloat(UAFuenteDatos.datos.listaVideos.count), texto: "Vídeo bajo demanda"),
            Segment(color: UIColor.yellow, value: CGFloat(UAFuenteDatos.datos.listaAudios.count), texto: "Audio")
            //Segment(color: UIColor.green, value: 3, texto: "Otros")
        ]
        pieChartView.titulo = "Plataforma de Televisón Online"
        pieChartView.autor = "Autor: Francisco J. Valero"
        self.view.addSubview(pieChartView)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

