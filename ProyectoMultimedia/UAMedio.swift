//
//  UaMedio.swift
//  ProyectoMultimedia
//
//  Created by Fran on 20/12/17.
//  Copyright © 2017 Fran. All rights reserved.
//

import Foundation
import UIKit
class UAMedio{
    var titulo : String
    var artista: String
    var album : String
    var url : URL
    var portada : UIImage
    
    init(_ titulo: String, _ artista: String, _ album: String, _ url: URL, _ portada: UIImage){
        self.titulo = titulo
        self.artista = artista
        self.album = album
        self.url = url
        self.portada = portada
    }
}
