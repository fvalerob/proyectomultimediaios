//
//  UAFuentesDatos.swift
//  ProyectoMultimedia
//
//  Created by Fran on 20/12/17.
//  Copyright © 2017 Fran. All rights reserved.
//

import Foundation
import UIKit
class UAFuenteDatos
{
    var listaAudios = [UAMedio]()
    var listaVideos = [UAMedio]()
    var listaEmisiones = [UAMedio]()
    
    private init() {
        let audio1URL = Bundle.main.url(forResource: "/sounds/pista1", withExtension: "mp3")
        let audio2URL = Bundle.main.url(forResource: "/sounds/pista2", withExtension: "mp3")
        let audio3URL = Bundle.main.url(forResource: "/sounds/pista3", withExtension: "mp3")
        let audio1 = UAMedio("Mack the Knife","Adlibitum","2018",audio1URL!, UIImage(named: "Placeholder.png")!)
        let audio2 = UAMedio("Mr. Zoo Suit","Adlibitum","2018",audio2URL!, UIImage(named: "Placeholder.png")!)
        let audio3 = UAMedio("Fly me Too the Moon","Adlibitum","2018",audio3URL!, UIImage(named: "Placeholder.png")!)
        
        self.listaAudios.append(audio1)
        self.listaAudios.append(audio2)
        self.listaAudios.append(audio3)
        
        let movie1URL = URL(string: "http://192.168.31.77:1935/vod/mp4:sample.mp4/playlist.m3u8")
        let video1 = UAMedio("Video 1 - SAMPLE Wowza","Adlibitum","2018",movie1URL!, UIImage(named: "Placeholder.png")!)
         let video2 = UAMedio("Video 2","Adlibitum","2018",movie1URL!, UIImage(named: "Placeholder.png")!)
         let video3 = UAMedio("Video 3","Adlibitum","2018",movie1URL!, UIImage(named: "Placeholder.png")!)
        self.listaVideos.append(video1)
        self.listaVideos.append(video2)
        self.listaVideos.append(video3)
        
        let directo1URL = URL(string: "http://192.168.31.77:1935/live/myStream/playlist.m3u8")
        let directo1 = UAMedio("MyStream","Adlibitum","2018",directo1URL!, UIImage(named: "Placeholder.png")!)
        let directo2URL = URL(string: "http://172.20.10.2:1935/live/canalA.stream/playlist.m3u8")
        let directo2 = UAMedio("Canal A","Adlibitum","2018",directo2URL!, UIImage(named: "Placeholder.png")!)
        let directo3URL = URL(string: "http://172.20.10.2:1935/live/canalB.stream/playlist.m3u8")
        let directo3 = UAMedio("Canal B","Adlibitum","2018",directo3URL!, UIImage(named: "Placeholder.png")!)
        let directo4URL = URL(string: "http://172.20.10.2:1935/live/canalC.stream/playlist.m3u8")
        let directo4 = UAMedio("Canal C","Adlibitum","2018",directo4URL!, UIImage(named: "Placeholder.png")!)
        let directo5URL = URL(string: "http://172.20.10.2:1935/live/canalD.stream/playlist.m3u8")
        let directo5 = UAMedio("Canal D","Adlibitum","2018",directo5URL!, UIImage(named: "Placeholder.png")!)
        
        self.listaEmisiones.append(directo1)
        self.listaEmisiones.append(directo2)
        self.listaEmisiones.append(directo3)
        self.listaEmisiones.append(directo4)
        self.listaEmisiones.append(directo5)
    }
    public static var sharedInstance : UAFuenteDatos {
        get { return datos }
    }
    static let datos : UAFuenteDatos = UAFuenteDatos()
    
    
    
}
